import { defineConfig } from 'astro/config';
import remarkMath from 'remark-math';
import rehypeMathJax from 'rehype-mathjax';

import mdx from "@astrojs/mdx";

// https://astro.build/config
export default defineConfig({
  site: "https://symphonious-moxie-777275.netlify.app/",
  markdown: {
    remarkPlugins: [remarkMath],
    rehypePlugins: [rehypeMathJax]
  },
  integrations: [mdx()]
});