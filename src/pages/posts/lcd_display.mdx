---
layout: ../../layouts/MarkdownPostLayout.astro
title: Interfacing with a Pmod LCD Display
author: Eric Quackenbush
description: "How to get a Pmod LCD display that originally came with a Renesas RX111 development board to work with an 8-bit PIC microcontroller through SPI."
pubDate: 2024-07-27
tags: ["embedded", "spi", "lcd", "pic"]
---
import ResponsiveImage from "../../components/ResponsiveImage.astro";
import pinout from '../../assets/pmod_lcd_display_pinout.svg';
import lcdFront from '../../assets/lcd_front.jpeg';
import lcdBack from '../../assets/lcd_back.jpeg';
import lcdPins from '../../assets/lcd_pins.jpeg';
import renesas from '../../assets/rsk_rx111_kit_0.jpg';
import renesasPinout from '../../assets/renesas_pinout.png';
import pmodSpec from '../../assets/pmod_spec.png';
import mplabPinGrid from '../../assets/mplab_pin_grid.png';
import helloWorld from '../../assets/hello_world.jpeg';

## Intro

The Pmod LCD display has no part numbers that I could find documentation for; however, I had discovered that it belongs to the Renesas RX111 starter kit. 

<ResponsiveImage
    mobileImgUrl={lcdFront}
    desktopImgUrl={lcdFront}
    alt="Front of the Pmod LCD Display"
/>

<ResponsiveImage
    mobileImgUrl={lcdBack}
    desktopImgUrl={lcdBack}
    alt="Back of the Pmod LCD Display"
/>

<ResponsiveImage
    mobileImgUrl={renesas}
    desktopImgUrl={renesas}
    alt="Renesas Starter Kit for RX111"
/>

## Pinout

On pg. 15 of the <a href="https://gitlab.com/equackenbush/lcd_display/-/blob/main/REN_r20ut2196eg0100_rskrx111_e2_studio_user_manual_MAT_20130612.pdf?ref_type=heads" target="_blank">user manual</a> for the RX111 Starter Kit, the pinout is shown:

<ResponsiveImage
    mobileImgUrl={renesasPinout}
    desktopImgUrl={renesasPinout}
    alt="Pinout for the Pmod LCD Display for the Renesas RX111"
/>

As one expects because this is a Pmod display, the pinout conforms to the <a href="https://gitlab.com/equackenbush/lcd_display/-/blob/main/pmod-interface-specification-1_2_0.pdf?ref_type=heads" target="_blank">Pmod spec</a> for a Pmod Interface Type 2A (expanded SPI):

<ResponsiveImage
    mobileImgUrl={pmodSpec}
    desktopImgUrl={pmodSpec}
    alt="Pmod spec for an expanded SPI inteface"
/>

I've created this helpful diagram for the pinout of the Pmod 2x6 male header since most diagrams, including in the spec, are for a female 2x6 header.

<ResponsiveImage
    mobileImgUrl={pinout}
    desktopImgUrl={pinout}
    alt="Pinout of the Pmod 2x6 male header"
/>

<ResponsiveImage
    mobileImgUrl={lcdPins}
    desktopImgUrl={lcdPins}
    alt="Pmod LCD Display Pins"
/>

Having the pinout is a start; however, we need to know what control signals and data we need to send the LCD for it to work. Let's turn to the sample code provided by Renesas.

## Renesas Sample Code

Looking through the sample code provided by Renesas, four files stand out:

- <a href="https://gitlab.com/equackenbush/lcd_display/-/blob/main/an_r01an1790eg0100_rx111_rsk/Workspace/RSKRX111_Samples_Cubesuite+/I2C_Master/cg_src/ascii.h?ref_type=heads" target="_blank">ascii.h</a>
- <a href="https://gitlab.com/equackenbush/lcd_display/-/blob/main/an_r01an1790eg0100_rx111_rsk/Workspace/RSKRX111_Samples_Cubesuite+/I2C_Master/cg_src/ascii.c?ref_type=heads" target="_blank">ascii.c</a>
- <a href="https://gitlab.com/equackenbush/lcd_display/-/blob/main/an_r01an1790eg0100_rx111_rsk/Workspace/RSKRX111_Samples_Cubesuite+/I2C_Master/cg_src/lcd.h?ref_type=heads" target="_blank">lcd.h</a>
- <a href="https://gitlab.com/equackenbush/lcd_display/-/blob/main/an_r01an1790eg0100_rx111_rsk/Workspace/RSKRX111_Samples_Cubesuite+/I2C_Master/cg_src/lcd.c?ref_type=heads" target="_blank">lcd.c</a>

These files contain the complete LCD driver code. Examining the #defines in the lcd.h header file we can figure out the mapping between the Renesas microcontroller pinout and the Pmod LCD pinout. I've commented which microcontroller pin corresponds to what Pmod LCD pin.

```c
/* DATA/COMMAND select pin */
#define DATA_CMD_PIN              (PORT4.PODR.BIT.B4)   // Pmod Pin 8

/* DATA/COMMAND select pin */
#define DATA_CMD_PIN_DIR          (PORT4.PDR.BIT.B4)

/* Pin defines common to both displays */

/* Display enable pin */
#define ENABLE_PIN                (PORTC.PODR.BIT.B7)   // Pmod Pin 1

/* Display enable pin */
#define ENABLE_PIN_DIR            (PORTC.PDR.BIT.B7)

/* reset pin */
#define RESET_PIN                 (PORTA.PODR.BIT.B3)   // Pmod Pin 8

/* reset pin pmr bit*/
#define RESET_PIN_PMR             (PORTA.PMR.BIT.B3)

/* Display enable pin */   
#define RESET_PIN_DIR             (PORTA.PDR.BIT.B3)

/* Vbat enable pin */
#define VBAT_ENABLE_PIN           (PORT4.PODR.BIT.B4)   // Pmod Pin 9

/* Vbat enable pin */
#define VBAT_ENABLE_PIN_DIR       (PORT4.PDR.BIT.B4)

/* Vdd enable pin */
#define VDD_ENABLE_PIN            (PORT4.PODR.BIT.B6)   // Pmod Pin 10

/* Vdd enable pin */
#define VDD_ENABLE_PIN_DIR        (PORT4.PDR.BIT.B6)

```

Now we can create a mapping between the Pmod LCD pins and the pins on the PIC16F15244 Curiosity Nano. Note that the Pmod Pin 7 is not used even though it is shown in the Renesas RX111 Starter Kit user manual as being connected to PA4.

| **Pmod Pin** | **μC Pin** | **Signal** | **Direction** |           **Name**           |
|:------------:|:----------:|:----------:|:-------------:|:----------------------------:|
|       1      |     RC7    |    GPIO    |      Out      | ENABLE_PIN                   |
|       2      |     RC5    |    SDO1    |      Out      | -                            |
|       3      |     RC5    |    SDI1    |       In      | -                            |
|       4      |     RC6    |    SCK1    |      Out      | -                            |
|       5      |     GND    |     GND    |       -       | -                            |
|       6      |     VTG    |     VCC    |       -       | -                            |
|       8      |     RA1    |    GPIO    |      Out      | RESET_PIN                    |
|       9      |     RA4    |    GPIO    |      Out      | DATA_CMD_PIN                 |
|              |            |            |               | VBAT_ENABLE_PIN              |
|       10      |     RA5    |    GPIO    |      Out      | VDD_ENABLE_PIN               |

Using MPLAB MCC to configure the pins, the pins should be configured as follows:
<ResponsiveImage
    mobileImgUrl={mplabPinGrid}
    desktopImgUrl={mplabPinGrid}
    alt="MPLAB Pin Grid View"
/>

We don't need to configure the GPIO pins since we take care of the configuration in the LCD driver.

## Modifying the LCD Driver Code

The general strategy is to remove any Renesas-specific stuff and replace it with the appropriate equivalents for the PIC16F15244.

Looking at acii.h there isn't anything that needs to be changed. However, with ascii.c there are two #pragma statements that are specific to the Renesas RX compiler that need to be removed.

```c
#pragma pack
...
#pragma unpack
```

In lcd.h we have to change the #defines for register expressions that are appropriate for the PIC16F15244:

```c
/* DATA/COMMAND select pin */
#define DATA_CMD_PIN              (LATAbits.LATA4)

/* DATA/COMMAND select pin */
#define DATA_CMD_PIN_DIR          (TRISAbits.TRISA4)

/* Pin defines common to both displays */

/* Display enable pin */
#define ENABLE_PIN                (LATCbits.LATC7)

/* Display enable pin */
#define ENABLE_PIN_DIR            (TRISCbits.TRISC7)

/* reset pin */
#define RESET_PIN                 (LATAbits.LATA1)

/* Display enable pin */   
#define RESET_PIN_DIR             (TRISAbits.TRISA1)

/* Vbat enable pin */
#define VBAT_ENABLE_PIN           (LATAbits.LATA4)

/* Vbat enable pin */
#define VBAT_ENABLE_PIN_DIR       (TRISAbits.TRISA4)

/* Vdd enable pin */
#define VDD_ENABLE_PIN            (LATAbits.LATA5)

/* Vdd enable pin */
#define VDD_ENABLE_PIN_DIR        (TRISAbits.TRISA5)
```

Notice that I have deleted the #define that sets the Port Register Mode for the Reset pin because there is no equivalent in the PIC world.

```c
/* reset pin pmr bit*/
#define RESET_PIN_PMR             (PORTA.PMR.BIT.B3)
```

Lastly, we turn to the lcd.c code. First thing we need to do is remove the Renesas specific imports.

```c
#include "r_cg_macrodriver.h" // Remove this line!
...
#include "r_cg_iodefine.h"

#include "r_cg_sci.h"
```

For the Init_LCD function, R_SCI5_Start() needs to be changed to SPI1_Open(0).

```c
/***********************************************************************************************************************
* Function Name : Init_LCD
* Description   : Initialises the LCD display. 
* Argument      : none
* Return value  : none
***********************************************************************************************************************/
void Init_LCD (void)
{
    /* Start SPI comm channel to LCD Display */
    SPI1_Open(0);

    /* initialise Standard PMOD display */
    init_pmod_lcd();

    /* clear the display before use */
    Clear_Display();
}
```

The nop() function needs to be changed to __delay_us(1).

```c
/***********************************************************************************************************************
* Function Name : DisplayDelay
* Description   : Delay routine for LCD or any other devices.
* Argument      : (uint32_t) units - time in, approximately, microsec
* Return value  : None
***********************************************************************************************************************/
void DisplayDelay (uint32_t const units)
{
    uint32_t counter = units * DELAY_TIMING;
    
    /* Decrement the counter and wait */
    while (counter--)
    {
        __delay_us(1);
    }
}
```

In the init_pmod_lcd() function, there's several changes that need to be made with the pin direction configurations. 

```c
/***********************************************************************************************************************
* Function Name : init_pmod_lcd
* Description   : Initialises the LCD display.
* Argument      : none
* Return value  : none
***********************************************************************************************************************/
static void init_pmod_lcd (void)
{
    /* Preset the output ports for i/o pins prior to setting as outputs */
    DATA_CMD_PIN = 0x1;
    ENABLE_PIN  = 0x1;
    RESET_PIN = 0x1;

    /* Configure the all involved pin directions as output */
    DATA_CMD_PIN_DIR = 0x0;
    ENABLE_PIN_DIR  = 0x0;
    RESET_PIN_DIR = 0x0; 

    DisplayDelay(1000);
    ....
}
```

The RESET_PIN_PMR expression can also be deleted because as was stated earlier there is no equivalent for the PIC.

```c
/* ensure that the port is i/o not peripheral (IRQ6) */
RESET_PIN_PMR = 0x0;   
```

With display_write_command() the function to write to the SPI buffer and wait for the TX to complete needs to be changed to the PIC equivalents:

```c
/***********************************************************************************************************************
* Function Name : display_write_command
* Description   : Encapsulation of autocode SPI5 send, sends 1 cmd byte.
* Argument      :
* Return value  : None
***********************************************************************************************************************/
static void display_write_command (uint8_t const cmd)
{
    /* data cmd pin low to indicate a command */
    DATA_CMD_PIN = 0x0;
    
    /* assert chip select */
    ENABLE_PIN  = 0x0;
    
    /*send command */
    SPI1_BufferWrite((uint8_t *)&cmd, 1);
    
    /* poll for tx complete */
    while (!SPI1_IsTxReady())
    {
        /* do nothing */
    }
    
    /* de-assert chip select */
    ENABLE_PIN  = 0x1;
    
    /* data cmd pin high to signify data */
    DATA_CMD_PIN = 0x1;
}
```

The same thing needs to be done in the display_write_data() command:

```c
/***********************************************************************************************************************
* Function Name : display_write_data
* Description   : Write a data byte to the display
* Argument      :
* Return value  : None
***********************************************************************************************************************/
static void display_write_data (uint8_t data)
{
    /* data cmd pin high to signify data */
    DATA_CMD_PIN = 0x1;
    
    /* assert chip select */
    ENABLE_PIN  = 0x0;
    
    /* send tx_num bytes */
    SPI1_BufferWrite((uint8_t *)&data, 1);
    /* poll for tx complete */
    while (!SPI1_IsTxReady())
    {
        /* do nothing */
    }
    /* de-assert chip select */
    ENABLE_PIN  = 0x1;
    
    /* data cmd pin high to signify data */
    DATA_CMD_PIN = 0x1;
}
```

That's it for the driver code.

## Hello World

With the driver code taken care of, we're finally ready to write a message to the LCD! In our main.c we need to add the LCD driver header file, initialize the LCD and then print our message.

```c
#include "mcc_generated_files/system/system.h"
#include "lcd.h"

/*
    Main application
*/

int main(void)
{
    SYSTEM_Initialize();

    // If using interrupts in PIC18 High/Low Priority Mode you need to enable the Global High and Low Interrupts 
    // If using interrupts in PIC Mid-Range Compatibility Mode you need to enable the Global and Peripheral Interrupts 
    // Use the following macros to: 

    // Enable the Global Interrupts 
    //INTERRUPT_GlobalInterruptEnable(); 

    // Disable the Global Interrupts 
    //INTERRUPT_GlobalInterruptDisable(); 

    // Enable the Peripheral Interrupts 
    //INTERRUPT_PeripheralInterruptEnable(); 

    // Disable the Peripheral Interrupts 
    //INTERRUPT_PeripheralInterruptDisable(); 

    // Initialize the debug LCD
    Init_LCD();
    
    // Display a message
    Display_LCD(1, "Hello World!");

    while(1)
    {

    }    
}

```

If everything worked correctly, you should see "Hello World!" print out on the LCD display:

<ResponsiveImage
    mobileImgUrl={helloWorld}
    desktopImgUrl={helloWorld}
    alt="Displaying a message on the LCD display"
/>

The MPLAB X project, the Renesas sample code and useful documentation can be found <a href="https://gitlab.com/equackenbush/lcd_display" target="_blank">here</a> on my GitLab.